"""vcard package"""
__author__ = "Victor Engmark"
__copyright__ = "Copyright (C) 2010-2021 Victor Engmark"
__credits__ = ["Victor Engmark", "Zearin"]
__maintainer__ = "Victor Engmark"
__email__ = "victor@engmark.name"
__license__ = "AGPLv3+"
__url__ = "https://gitlab.com/victor-engmark/vcard"
__version__ = "0.15.0"
