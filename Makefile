NAME = $(notdir $(CURDIR))

PYTHON = python

# Release
GPG_ID ?= EC92D395260D3194

# Git
GIT := git
GIT_TAG = $(GIT) tag -au $(GPG_ID)

# Python
SETUP = setup.py
INSTALL_OPTIONS := -O2
UPLOAD_OPTIONS = --sign --identity=$(GPG_ID)

# System
RM := rm -f

build_directory = $(CURDIR)/build
distribution_directory = $(CURDIR)/dist

uid = $(shell id -u)

.PHONY: all
all: distribute

.PHONY: test
test:
	pre-commit run --all-files
	PYTHONPATH=vcard coverage run setup.py test
	coverage report
	coverage xml

.PHONY: test-clean
test-clean:
	# Run after `make clean`
	test -z "$$($(GIT) clean --dry-run -dx | tee /dev/stderr)"

.PHONY: install
install:
	$(PYTHON) $(SETUP) install $(INSTALL_OPTIONS)
	for dir in /etc/bash_completion.d /usr/share/bash-completion/completions; \
	do \
		if [ -d "$$dir" ]; \
		then \
			install --mode 644 bash-completion/$(NAME) "$$dir" || exit 1; \
			break; \
		fi; \
	done

.PHONY: distribute
distribute: $(distribution_directory)
	poetry build

.PHONY: release
release: distribute
	twine upload $(distribution_directory)/*
	$(GIT_TAG) -m 'PyPI release' v$(python version.py)
	@echo 'Remember to `git push --tags`'

$(build_directory) $(distribution_directory):
	mkdir $@

.PHONY: clean
clean: clean-build clean-ci clean-dist clean-test

.PHONY: clean-build
clean-build: clean-build-third-party clean-build-local

.PHONY: clean-build-third-party
clean-build-third-party:
	-$(RM) -r $(build_directory)

.PHONY: clean-build-local
clean-build-local:
	-$(RM) -r  .mypy_cache .venv $(NAME).egg-info vcard/__pycache__

.PHONY: clean-ci
clean-ci:
	-$(RM) -r .cache

.PHONY: clean-dist
clean-dist:
	-$(RM) -r $(distribution_directory)

.PHONY: clean-test
clean-test:
	-$(RM) -r .coverage coverage.xml test/__pycache__

include make-includes/variables.mk
include make-includes/xml.mk
