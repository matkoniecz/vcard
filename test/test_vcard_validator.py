from unittest import TestCase
from unittest.mock import MagicMock, patch

from vcard import vcard_validator


class TestVcardValidator(TestCase):
    @patch("vcard.vcard_validator.validate_file")
    def test_result_from_validator(self, validate_file_mock: MagicMock) -> None:
        validate_file_mock.return_value = "foo"

        validator = vcard_validator.VcardValidator("/some/path", False)

        self.assertEqual(validator.result, "foo")
