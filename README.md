# [vCard module](https://gitlab.com/victor-engmark/vcard)

[![pipeline status](https://gitlab.com/victor-engmark/vcard/badges/master/pipeline.svg)](https://gitlab.com/victor-engmark/vcard/commits/master)
[![Python: 3.6-3.9](https://img.shields.io/badge/Python-3.6--3.9-blue)](https://www.python.org/)
[![coverage report](https://gitlab.com/victor-engmark/vcard/badges/main/coverage.svg)](https://gitlab.com/victor-engmark/vcard/-/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pylint: passing](https://img.shields.io/badge/pylint-passing-brightgreen)](https://www.pylint.org/)
[![shellcheck: passing](https://img.shields.io/badge/shellcheck-passing-brightgreen)](https://www.shellcheck.net/)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

This program can be used for strict validation and parsing of vCards. It currently supports [vCard 3.0 (RFC 2426)](https://tools.ietf.org/html/rfc2426).

Additional scripts:

- [`format-TEL.sh`](./format-TEL.sh) - Format phone numbers according to national standards
- [`split.sh`](./split.sh) - Split a multiple vCards file into individual files
- [`sort-lines.sh`](./sort-lines.sh) - Sort vCard property lines according to a custom key
- [`join-lines.sh`](./join-lines.sh) - Join previously split vCard lines
- [`split-lines.sh`](./split-lines.sh) - Split long vCard lines

## Installation / upgrade

    sudo pip install --upgrade vcard

## Development

**Download:**

    git clone --recurse-submodules git@gitlab.com:victor-engmark/vcard.git

**Release:**

1. Bump the [version](vcard/__init__.py).
2. Commit everything.
3. `make clean test-clean test release`
4. `git push && git push --tags`

Development requirements:

- GNU Make

Release requirements:

- GnuPG
